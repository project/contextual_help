<?php

declare(strict_types=1);

namespace Drupal\contextual_help\Plugin\Mode;

use Drupal\navigation_plus\Attribute\Mode;
use Drupal\navigation_plus\ModeInterface;
use Drupal\navigation_plus\ModePluginBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Help mode plugin.
 */
#[Mode(
  id: 'help',
  label: new TranslatableMarkup('Help'),
  weight: 10,
)]
final class Help extends ModePluginBase {

  static private ?array $helpTopics = NULL;

  private function getHelpTopics() {
    if (is_null(self::$helpTopics)) {
      self::$helpTopics = \Drupal::service('contextual_help')->getHelpTopicsForNavigationModule();
    }
    return self::$helpTopics;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(): bool {
    return !empty($this->getHelpTopics());
  }

  /**
   * {@inheritdoc}
   */
  public function getIconPath(): string {
    $path = $this->extensionList->getPath('navigation');
    return "/$path/assets/icons/help.svg";
  }

  /**
   * {@inheritdoc}
   */
  public function buildToolbarItems(array &$variables, ModeInterface $mode): array {
    // Add help options.
    return self::$helpTopics;
  }

}
