<?php

namespace Drupal\contextual_help;

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Help plugin interface.
 */
interface HelpInterface {

  /**
   * Applies.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   *
   * @return bool
   *   Whether or not the help plugin applies to this page.
   */
  public function applies(RouteMatchInterface $route_match): bool;

  /**
   * Display.
   *
   * @return array
   *   A renderable array of help content.
   */
  public function display(): array;

}
