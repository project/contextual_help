<?php

namespace Drupal\contextual_help;

use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;

/**
 * Help plugin manager.
 */
class HelpManager extends DefaultPluginManager {

  private RouteMatchInterface $routeMatch;

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
    parent::__construct(
      'Plugin/Help',
      $namespaces,
      $module_handler,
      'Drupal\contextual_help\HelpInterface',
      'Drupal\contextual_help\Annotation\Help',
    );
    $this->alterInfo('contextual_help_info');
    $this->setCacheBackend($cache_backend, 'contextual_help_info_plugins');
  }

  /**
   * Get help topics.
   *
   * @return array
   *   An unordered list of links to help plugin modals for this page.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getHelpTopics(): array {
    $help_topic_definitions = $this->getDefinitions();
    usort($help_topic_definitions, function ($a, $b) {
      return $a['weight'] <=> $b['weight'];
    });
    $help_topics = [];
    foreach ($help_topic_definitions as $definition) {
      $instance = $this->createInstance($definition['id']);
      if ($instance->applies($this->routeMatch)) {
        $help_topics[$definition['id']] = $this->getHelpLink($definition);
      }
    }

    return empty($help_topics) ? $help_topics : [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $help_topics,
    ];
  }

  /**
   * Get a link to the help plugin text by plugin definition.
   *
   * @param $definition
   *   The plugin definition.
   *
   * @return array
   *   The build array defining the link.
   */
  public function getHelpLink($definition) {
    return [
      '#type' => 'link',
      '#title' => $definition['title'],
      '#url' => Url::fromRoute('contextual_help.tray', ['plugin_id' => $definition['id']]),
      '#options' => ['query' => \Drupal::destination()->getAsArray()],
      '#attributes' => [
        'class' => ['new', 'use-ajax'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode([
          'width' => 900,
        ]),
      ],
    ];
  }

  /**
   * Get help topics.
   *
   * @return array
   *   An unordered list of links to help plugin modals for this page.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getHelpTopicsForNavigationModule(): array {
    $help_topic_definitions = $this->getDefinitions();
    usort($help_topic_definitions, function ($a, $b) {
      return $a['weight'] <=> $b['weight'];
    });
    $help_topics = [];
    foreach ($help_topic_definitions as $definition) {
      $instance = $this->createInstance($definition['id']);
      if ($instance->applies($this->routeMatch)) {
        $help_topics[$definition['id']] = $this->getHelpLinkForNavigationModule($definition);
      }
    }

    return $help_topics;
  }

  /**
   * Get a link to the help plugin text by plugin definition.
   *
   * @param $definition
   *   The plugin definition.
   *
   * @return array
   *   The build array defining the link.
   */
  public function getHelpLinkForNavigationModule($definition) {
    return [
      '#type' => 'link',
      '#title' => Markup::create('<span class="toolbar-button__label">' . $definition['title'] . '</span>'),
      '#url' => Url::fromRoute('contextual_help.tray', ['plugin_id' => $definition['id']]),
      '#options' => ['query' => \Drupal::destination()->getAsArray()],
      '#attributes' => [
        'class' => ['new', 'use-ajax', 'toolbar-button', 'toolbar-button--collapsible', 'navigation-plus-button'],
        'data-icon-text' => mb_substr($definition['title'], 0, 2, "UTF-8"),
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode([
          'width' => 900,
        ]),
      ],
      '#wrapper_attributes' => [
        'class' => ['toolbar-block__list-item'],
      ],
    ];
  }

  /**
   * Get a link to the help plugin text by plugin id.
   *
   * @param string $plugin_id
   *   The requested plugin id.
   *
   * @return array
   *   The build array defining the link.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getHelpLinkById(string $plugin_id) {
    if (!$this->hasDefinition($plugin_id)) {
      throw new PluginNotFoundException($plugin_id);
    }
    $definition = $this->getDefinition($plugin_id);
    return $this->getHelpLink($definition);
  }

}
