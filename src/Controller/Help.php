<?php

namespace Drupal\contextual_help\Controller;

use Drupal\contextual_help\HelpManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Contextual Help controller.
 */
class Help implements ContainerInjectionInterface {

  use StringTranslationTrait;

  private HelpManager $manager;

  public function __construct(HelpManager $help_manager) {
    $this->manager = $help_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('contextual_help')
    );
  }

  /**
   * Title.
   *
   * The help modal title from plugin ID.
   *
   * @param string $plugin_id
   *   The help plugin ID.
   *
   * @return mixed|string
   *   The help modal title.
   */
  public function title(string $plugin_id) {
    try {
      return $this->manager->getDefinition($plugin_id)['title'];
    }
    catch (PluginNotFoundException $e) {
      return '404';
    }
  }

  /**
   * Modal.
   *
   * Gets the modal content from the help plugin.
   *
   * @param string $plugin_id
   *   The help plugin ID.
   *
   * @return array
   *   The help content.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function modal(string $plugin_id) {
    try {
      return $this->manager->createInstance($plugin_id)->display();
    }
    catch (PluginNotFoundException $e) {
      return ['#markup' => $this->t('The "%plugin_id" help doesn\'t exist.', ['%plugin_id' => $plugin_id])];
    }
  }

}
