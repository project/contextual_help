<?php

namespace Drupal\contextual_help\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Help annotation object.
 *
 * @ingroup help
 *
 * @Annotation
 */
class Help extends Plugin {

  /**
   * The help plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the Help plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $title;


  /**
   * The plugin weight for sorting.
   *
   * @var string
   */
  public $weight;

}
