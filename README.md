# Contextual Help

Provides pluggable toolbar help topics. Simply add a plugin to `/src/Plugin/Help/MyHelpTopic.php`.

#### Example plugin
```php
<?php

namespace Drupal\MY_MODULE\Plugin\Help;

use Drupal\contextual_help\HelpInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Clear Caches.
 *
 * @help(
 *   id = "clear_caches",
 *   title = @Translation("How to clear caches"),
 *   weight = 10,
 * )
 */
class MyHelpTopic implements HelpInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match): bool {
    return $route_match->getRouteName() === 'system.admin_content';
  }

  /**
   * {@inheritdoc}
   */
  public function display(): array {
    return [
      '#markup' => t('<p>Navigate Manage > Configuration > Development > Performance and click "Clear all caches".</p>'),
    ];
  }

}

```
